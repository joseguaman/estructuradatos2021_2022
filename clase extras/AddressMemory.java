import java.lang.reflect.Field;
import sun.misc.Unsafe;

/**
 *
 * @author sebas
 */
public class AddressMemory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String valor = "casa";
        String valor1 = "casa2";
        printAddresses(valor, valor);
        printAddresses(valor1, valor1);
        String[] cadenas = {"uno", valor, valor1,"etc"};
        printAddresses("arreglo", cadenas);

    }

    public static Unsafe getUnsafe() {
        try {
            Field f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            return (Unsafe) f.get(null);
        } catch (Exception e) {
            return null;
        }
    }

    public static void printAddresses(String label, Object... objects) {
        System.out.print(label + ": 0x");
        Unsafe unsafe = getUnsafe();
        boolean is64bit = true;
        long last = 0;
        int offset = unsafe.arrayBaseOffset(objects.getClass());
        int scale = unsafe.arrayIndexScale(objects.getClass());
        switch (scale) {
            case 4:
                long factor = is64bit ? 8 : 1;
                final long i1 = (unsafe.getInt(objects, offset) & 0xFFFFFFFFL) * factor;
                System.out.print(Long.toHexString(i1));
                last = i1;
                for (int i = 1; i < objects.length; i++) {
                    final long i2 = (unsafe.getInt(objects, offset + i * 4) & 0xFFFFFFFFL) * factor;
                    if (i2 > last) {
                        System.out.print(", +" + Long.toHexString(i2 - last));
                    } else {
                        System.out.print(", -" + Long.toHexString(last - i2));
                    }
                    last = i2;
                }
                break;
            case 8:
                throw new AssertionError("Not supported");
        }
        System.out.println();
    }
}
